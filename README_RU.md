Всавьте код на вашу страницу

`require_once 'redirector/Router.php';` 

`$objRedirector = new Router('PATH', 'DEFAULT_LINK');` 

`$objRedirector->start();` 

Аргументы к объекту 'Router': 

PATH = путь и полное имя файла со ссылками, пример: 'links/links.csv'

DEFAULT_LINK = линк на который идет редирект после отключения всех ссылок в файле, пример: 'http://mysite.com' 

Вставьте ваш код аналитики в файл streak.php. по умолчанию тайм-аут редиректа = 3сек.

**Поддерживаемые типы файлов:**

1. 'txt'. пример синтаксиса: 

``0`http://link1``

``;0`http://link2``

``;1`http://link3``

where 1 or 0 = status link, 0 = allready disable link.

2. 'csv'. (первая строка мгнорируется, разделитель кома = ',') пример синтаксиса: 

``status,url``

``0,http://link1``

``1,http://link2``

``1,http://link3``

``1,http://link4``
