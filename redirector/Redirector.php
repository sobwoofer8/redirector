<?php
/**
 * Created by PhpStorm.
 * User: sobwoofer
 * Date: 10.02.17
 * Time: 20:26
 */

namespace redirector;


class Redirector
{

    /**
     * @param $redirectTo
     */
    public function redirectTo($redirectTo)
    {
        header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        ob_start();
        $link = str_replace("\n", "", $redirectTo);
        require_once 'streak.php';
        ob_end_flush();
    }

}