<?php
namespace redirector;
use redirector\drivers\DriverCSV;
use redirector\drivers\DriverTXT;
require_once 'Redirector.php';
require_once 'drivers/DriverCSV.php';
require_once 'drivers/DriverTXT.php';

class Router
{
    private $path = '';
    private $driver = '';
    private $redirectTo = '';
    public $data = [];

    /**
     * Router constructor.
     * @param $pathString
     * @param $typeFile
     * @param $defaultPage
     */
    function __construct($pathString, $defaultPage)
    {
        $this->rules($pathString);
        $this->redirectTo = $defaultPage;
    }

    /**
     * @param $typeFile
     */
    private function rules($pathString)
    {
        $pathArray = explode('.', $pathString);
        $typeFile = strtoupper(end($pathArray));
        $this->path = $pathString;

        $supportTypes = ['TXT', 'CSV'];
        if (in_array($typeFile, $supportTypes)) {
            $this->driver = "redirector\\drivers\\Driver$typeFile";
        }
    }

    public function start()
    {
        $this->getLinksFromFile();
        $driver = new $this->driver;
        $result = $driver->getCurrentLink($this->data);

        if($result){
            $this->data = $result['links'];
            $this->saveLinksFromFile();
            $this->redirectTo = $result['redirectTo'];
        }

        $redirector = new Redirector();
        $redirector->redirectTo($this->redirectTo);
    }

    private function getLinksFromFile()
    {
        $driver = new $this->driver;
        $result = $driver->getArray($this->path);

        $this->data = $result;
    }

    private function saveLinksFromFile()
    {
        $driver = new $this->driver;
        $driver->save($this->path, $this->data);

    }




}