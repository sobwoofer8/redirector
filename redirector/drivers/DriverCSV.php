<?php
namespace redirector\drivers;


class DriverCSV
{
    public function getArray($path)
    {
        $resource = fopen($path, 'r');
        $data = [];
        while (($str = fgetcsv($resource)) != false ) {
            $data[] = $str;
        }
        fclose($resource);
        return $data;
    }

    public function getCurrentLink($links)
    {
        for($i=1; $i<=count($links); $i++) {
            if ($links[$i][0] == '1'){
                $links[$i][0] = '0';
                $result = [
                    'redirectTo' => $links[$i][1],
                    'links' => $links
                ];
                return $result;
            }
        }
        return false;
    }

    /**
     * @param $path
     * @param $links
     */
    public function save($path, $links)
    {
        $resource = fopen($path, 'w');
        foreach ($links as $link) {
            fputcsv($resource, $link);
        }
    }




}