<?php
namespace redirector\drivers;


class DriverTXT
{
    /**
     * @param $path
     * @return array
     */
    public function getArray($path)
    {
        $textLink = file_get_contents($path);
        $fileArray = explode(';', $textLink);
        $linksResult = [];
        foreach ($fileArray as $item){
            $itemArray = explode('`', $item);
            $linksResult[] = $itemArray;
        }
        return $linksResult;
    }

    /**
     * @param $path
     * @param $links
     */
    public function save($path, $links)
    {
        $sepOne = [];
        foreach ($links as $link) {
            $sepOne[] = implode('`', $link);
        }
        $result = implode(';', $sepOne);
        file_put_contents($path, $result);
    }

    /**
     * @param $links
     * @return array|bool
     */
    public function getCurrentLink($links)
    {
        foreach ($links as $key => $link){
            if (isset($link[0]) && $link[0] === '1') {
                $links[$key][0] = '0';
                $result = [
                    'links' => $links,
                    'redirectTo' => $links[$key][1]
                ];
                return $result;
            }
        }
        return false;
    }
}