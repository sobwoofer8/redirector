insert code to your page 

`require_once 'redirector/Router.php';` 

`$objRedirector = new Router('PATH', 'DEFAULT_LINK');` 

`$objRedirector->start();` 

where argument in object 'Router': 

PATH = path and full name file with links, example 'links/links.csv'

DEFAULT_LINK = link redirecting after disable all links in file, example 'http://mysite.com' 

insert your analytics code to streak.php. Default redirect time out = 3sec.

**Supported types files:**

1. 'txt'. example sintax: 

``0`http://link1``

``;0`http://link2``

``;1`http://link3``

where 1 or 0 = status link, 0 = allready disable link.

2. 'csv'. (first line ignored, delimiter = ',') example sintax: 

``status,url``

``0,http://link1``

``1,http://link2``

``1,http://link3``

``1,http://link4``
